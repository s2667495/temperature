package nl.utwente.di.temperature;

public class Converter {
    public double getFahrenheit(double celsius) {
        return (celsius * 9/5) + 32;
    }
}