package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/** Tests the Converter **/
public class TestConverter {
    @Test
    public void testConverter() throws Exception {
        Converter converter = new Converter();
        double price = converter.getFahrenheit(20);
        Assertions.assertEquals(68, price, "temperature 20c");
    }
}